﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiveChat.ViewModels
{
    public class Period
    {
        public string period1 { get; set; }
        public string period2 { get; set; }
    }
}
