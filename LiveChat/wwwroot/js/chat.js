﻿// Инициализация
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/chathub")
    .configureLogging(signalR.LogLevel.Information)
    .build();

// Установка соединения
try {
    connection.start();
    console.log("SignalR Connected.");
} catch (err) {
    console.log(err);
}

// Добавил позмодность отправлять сообщение с кнопки "Enter"
$(document).keypress(function (e) {
    if (e.which === 13) {
        $("#sendButton").click();
    }
});

// Инициализация инпутов для дат
$(function () {
    $('#datetimepicker7').datetimepicker();
    $('#datetimepicker8').datetimepicker({
        useCurrent: false
    });
    $("#datetimepicker7").on("change.datetimepicker", function (e) {
        $('#datetimepicker8').datetimepicker('minDate', e.date);
    });
    $("#datetimepicker8").on("change.datetimepicker", function (e) {
        $('#datetimepicker7').datetimepicker('maxDate', e.date);
    });
});

// Событие срабатывает при вызове на сервере метода "Notify"
connection.on("Notify",
    function (message) {
        var onlineList = JSON.parse(message);
        var usersItems = document.getElementById("userList").children;

        for (var i = 0; i < usersItems.length; i++) {
            usersItems[i].style.borderLeft = 'none';
        }
        onlineList.forEach(onlineUser);
        function onlineUser(item, index) {
            document.getElementById(item.userId).style.borderLeft = "5px green solid";
        }
    });

// Рассоединение при закрытии окна браузера
window.onbeforeunload = function () {
    connection.hub.stop();
    return;
};

// Получение списка пользователей
connection.on("GetUserList",
    function (users, onlineUsers, currentUserId) {
        var userList = JSON.parse(users);
        userList.forEach(user);
        document.getElementById("back-to").style.visibility = 'collapse';
        function user(item, index) {
            var userListHtml = document.getElementById("userList");
            var lastMessage = "";
            if (item.LAST_MESSAGE != null) {
                lastMessage = item.LAST_MESSAGE;
            }
            if (item.USER_ID === currentUserId) {
                userListHtml.insertAdjacentHTML('afterbegin', '<div class="display_none list-group user_item" id=' + item.USER_ID + '> <a href = "#" class= "list-group-item list-group-item-action active user-link" > <div class="d-flex w-100 justify-content-between"> <h5 class= "mb-1 user-name"> ' + item.NAME + '</h5 > <small class="last-message-time">3 days ago</small></div> <p class="mb-1 last-message" id="clientLastMessage">' + lastMessage + '</p> </a > </div >');
                document.getElementById("message_count").innerHTML = "Количество сообщений: " + item.MESSAGE_COUNT;
                document.getElementById("user_name_avatar").innerHTML = item.NAME;
            } else {
                userListHtml.insertAdjacentHTML('afterbegin', '<div class="list-group user_item" id=' + item.USER_ID + '> <a href = "#" class= "list-group-item list-group-item-action active user-link" > <div class="d-flex w-100 justify-content-between"> <h5 class= "mb-1 user-name"> ' + item.NAME + '</h5 > <small class="last-message-time">3 days ago</small></div> <p class="mb-1 last-message" id="clientLastMessage">' + lastMessage + '</p> </a > </div >');
                document.getElementById(item.USER_ID).addEventListener("click",
                function(event) {
                    connection.invoke("GetSpecificUserMessages", item.USER_ID).catch(function (err) {
                        return console.error(err.toString());
                    });
                    document.getElementById('back-to').style.visibility = 'visible';
                    document.getElementById('head-user-name').innerHTML = "Сообщения пользователя " + item.NAME;
                });

            }
        }
        document.getElementById('head-user-name').innerHTML = "Общий чат";
        var onlineList = JSON.parse(onlineUsers);
        onlineList.forEach(onlineUserSetStyle);
        function onlineUserSetStyle(item, index) {
            document.getElementById(item.userId).style.borderLeft = "5px green solid";
        }
    });

// Получение списка пользователей по заданным пераметра (захардкожено на сервере)
connection.on("GetSpecificUserMessages", function (messagesHistory, currentUser) {
    var messageContainer = document.getElementById("body-chat");
    var data = JSON.parse(messagesHistory);
    var currentUserJson = JSON.parse(currentUser);
    messageContainer.innerHTML = "";
    for (var i = 0; i < data.length; i++) {
        messageContainer.insertAdjacentHTML('beforeend', '<div class="receive_message_block"><h4>' + data[i].USER_NAME + '</h4><div class="receive-message"><div class="rect_receive"></div><div><p>' + data[i].BODY + '</p><div class="date_receive_message">' + getDate(data[i].TIMESTAMP) + '</div></div></div></div>');
    }
    messageContainer.scrollTop = messageContainer.scrollHeight;
});

// Получение списка сообщений
connection.on("GetMessageList",
    function (messagesHistory, currentUser) {
        var messageContainer = document.getElementById("body-chat");
        var data = JSON.parse(messagesHistory);
        var currentUserJson = JSON.parse(currentUser);
        messageContainer.innerHTML = "";
        for (var i = 0; i < data.length; i++) {
            if (data[i].SENDER_ID === currentUserJson.USER_ID) {
                messageContainer.insertAdjacentHTML('beforeend', '<div class="send_message_block"><div class="send-message"><div><p>' + data[i].BODY + '</p><div class="date_send_message">' + getDate(data[i].TIMESTAMP) + '</div></div><div class="rect_send"></div></div></div>');
            } else {
                messageContainer.insertAdjacentHTML('beforeend', '<div class="receive_message_block"><h4>' + data[i].USER_NAME + '</h4><div class="receive-message"><div class="rect_receive"></div><div><p>' + data[i].BODY + '</p><div class="date_receive_message">' + getDate(data[i].TIMESTAMP) + '</div></div></div></div>');
            }
        }
        messageContainer.scrollTop = messageContainer.scrollHeight;
    });

// Получение актуальной информации о активном пользователе
connection.on("GetActualInformation", function(userInformation) {
    var userInformationParsed = JSON.parse(userInformation);
    
    document.getElementById("message_count").innerHTML = "Количество сообщений: " + userInformationParsed.MESSAGE_COUNT;
});

// Получение нового сообщения
connection.on("ReceiveMessage", function (user, message) {
    var messageContainer = document.getElementById("body-chat");
    var userInfo = JSON.parse(user);
    var messageJson = JSON.parse(message);
    messageContainer.insertAdjacentHTML('beforeend', '<div class="receive_message_block"><h4>' + userInfo.NAME + '</h4><div class="receive-message"><div class="rect_receive"></div><div><p>' + messageJson.BODY + '</p><div class="date_receive_message">' + getDate(messageJson.TIMESTAMP) + '</div></div></div></div>');

    var children = document.getElementById(userInfo.USER_ID).children[0].children[1];

    children.innerHTML = userInfo.LAST_MESSAGE;

    messageContainer.scrollTop = messageContainer.scrollHeight;
});

// Отправка сообщений
document.getElementById("sendButton").addEventListener("click", function (event) {
    var messageInput = document.getElementById("messageInput");
    var message = messageInput.value;
    
    if (message !== "") {
        var messageContainer = document.getElementById("body-chat");
        connection.invoke("SendMessage", message).catch(function (err) {
            return console.error(err.toString());
        });

        messageContainer.insertAdjacentHTML('beforeend', '<div class="send_message_block"><div class="send-message"><div><p>' + message + '</p><div class="date_send_message">' + getDate(new Date()) + '</div></div><div class="rect_send"></div></div></div>');
        messageContainer.scrollTop = messageContainer.scrollHeight;
        document.getElementById("messageInput").value = "";
        event.preventDefault();
    }
});

// Получение всех сообщений
document.getElementById("back-to").addEventListener("click", function(event) {
    connection.invoke("GetSpecificUserMessages", "all").catch(function (err) {
        return console.error(err.toString());
    });
    document.getElementById('head-user-name').innerHTML = "Общий чат";
    this.style.visibility = 'collapse';
});


// Получение сообщений за сегодня
document.getElementById("today-button").addEventListener("click", function (event) {
    connection.invoke("GetSpecificUserMessages", "today").catch(function (err) {
        return console.error(err.toString());
    });
    document.getElementById('head-user-name').innerHTML = "Сегодняшние сообщения";
    document.getElementById('back-to').style.visibility = 'visible';
});

// Получение сообщений за вчера
document.getElementById("yesterday-button").addEventListener("click", function (event) {
    connection.invoke("GetSpecificUserMessages", "yesterday").catch(function (err) {
        return console.error(err.toString());
    });
    
    document.getElementById('head-user-name').innerHTML = "Вчерашние сообщения";
    document.getElementById('back-to').style.visibility = 'visible';
});

// Фильтрация сообщений по дате
document.getElementById('filter').addEventListener("click", function (event) {
    var periods = {
        period1: document.getElementById('period1').value,
        period2: document.getElementById('period2').value
    }
    connection.invoke("GetSpecificUserMessages", JSON.stringify(periods)).catch(function (err) {
        return console.error(err.toString());
    });

    document.getElementById('head-user-name').innerHTML = "Сообщения за период";
    document.getElementById('back-to').style.visibility = 'visible';
});

// Разветка форм установки фильтров
document.getElementById("data-filter-toggle").addEventListener("click", function (event) {
    var datefields = document.getElementById("date-filter-container");
    if (datefields.style.display === 'none') {
        datefields.style.display = 'block';
    } else {
        datefields.style.display = 'none';
    }
});

// Получение читабельной даты
function getDate(dateTime) {
    var messageDate = moment(dateTime);
    var month = [
        "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня",
        "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"
    ][messageDate.month()];
    return messageDate.date() + ' ' + month + " " + messageDate.hours() + ":" + messageDate.format('M');
}