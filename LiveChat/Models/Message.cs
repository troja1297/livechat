﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LiveChat.Models
{
    public class Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MESSAGE_ID { get; set; }
        public string BODY { get; set; }
        public string SENDER_ID { get; set; }
        public string USER_NAME { get; set; }
        public DateTime TIMESTAMP { get; set; }

        [JsonIgnore]
        public User Sender { get; set; }
    }
}
