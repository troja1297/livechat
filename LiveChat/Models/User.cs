﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace LiveChat.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string USER_ID { get; set; }
        public string EMAIL { get; set; }
        public string NAME { get; set; }

        public string PASSWORD { get; set; }
        public string LAST_MESSAGE { get; set; }
        public int MESSAGE_COUNT { get; set; }
        public IList<Message> Messages { get; set; }
    }
}
