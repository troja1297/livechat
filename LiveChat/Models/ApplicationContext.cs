﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace LiveChat.Models
{
    public sealed class ApplicationDbContext : DbContext
    {
        private Random rnd = new Random();
        public Microsoft.EntityFrameworkCore.DbSet<User> Users { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Message> Messages { get; set; }
        public static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter((category, level) =>
                        category == DbLoggerCategory.Database.Command.Name
                        && level == LogLevel.Debug)
                    .AddConsole();
            });

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseLoggerFactory(MyLoggerFactory);

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Конфигурирует таблицы бд и заполняет некоторые данными.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>()
                .HasOne<User>(s => s.Sender)
                .WithMany(s => s.Messages)
                .HasForeignKey(s => s.SENDER_ID)
                .IsRequired();

            modelBuilder.Entity<User>().HasData(
                new User()
                {
                    USER_ID = "e89db13f-7c9b-4907-90f0-83a47b21c50b",
                    EMAIL = "trojan1290@gmail.com",
                    NAME = "Влад Андреев",
                    PASSWORD = "trojan1290@gmail.com"
                },
                new User()
                {
                    USER_ID = "c52e9cf5-e074-4b3f-84dc-ff5d1bf5f126",
                    EMAIL = "trojan1291@gmail.com",
                    NAME = "Роман Ширяев",
                    PASSWORD = "trojan1291@gmail.com"
                },
                new User()
                {
                    USER_ID = "0dcfef1c-f2da-4263-960b-812039338c5e",
                    EMAIL = "trojan1292@gmail.com",
                    NAME = "Владимир Фомичев",
                    PASSWORD = "trojan1292@gmail.com"
                },
                new User()
                {
                    USER_ID = "50eb1c17-0aa6-4b95-ab5c-54d679385e7c",
                    EMAIL = "trojan1293@gmail.com",
                    NAME = "Андрей Якушев",
                    PASSWORD = "trojan1293@gmail.com"
                },
                new User()
                {
                    USER_ID = "108c99a3-bba6-4b88-8423-07ebb28aee26",
                    EMAIL = "trojan1294@gmail.com",
                    NAME = "Николай Гришин",
                    PASSWORD = "trojan1294@gmail.com"
                },
                new User()
                {
                    USER_ID = "34a3c4cd-a272-42cf-b267-f11e0e34a7c2",
                    EMAIL = "trojan1295@gmail.com",
                    NAME = "Лев Селиверстов",
                    PASSWORD = "trojan1295@gmail.com"
                },
                new User()
                {
                    USER_ID = "565c61d1-f409-4732-a34f-5c56113c2a0e",
                    EMAIL = "trojan1296@gmail.com",
                    NAME = "Николай Егоров",
                    PASSWORD = "trojan1296@gmail.com"
                },
                new User()
                {
                    USER_ID = "cc4b3bde-29ef-4938-b0f8-c3006fbcbe54",
                    EMAIL = "trojan1297@gmail.com",
                    NAME = "Тимур Гиряев",
                    PASSWORD = "trojan1297@gmail.com"
                },
                new User()
                {
                    USER_ID = "54927a7f-f3da-497e-8ded-49c159860e0a",
                    EMAIL = "trojan1298@gmail.com",
                    NAME = "Сергей Дроздов",
                    PASSWORD = "trojan1298@gmail.com"
                },
                new User()
                {
                    USER_ID = "c97cb19c-3839-4961-a7a1-c12a6a4c66f8",
                    EMAIL = "trojan1299@gmail.com",
                    NAME = "Алексей Кондратьев",
                    PASSWORD = "trojan1299@gmail.com"
                }
            );
        }
    }
}
