﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LiveChat.Migrations
{
    public partial class AddMessageCountAndLastMessageFieldsToUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LAST_MESSAGE",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MESSAGE_COUNT",
                table: "Users",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LAST_MESSAGE",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "MESSAGE_COUNT",
                table: "Users");
        }
    }
}
