﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LiveChat.Migrations
{
    public partial class AddUserNameFieldToMessageTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "USER_NAME",
                table: "Messages",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "USER_NAME",
                table: "Messages");
        }
    }
}
