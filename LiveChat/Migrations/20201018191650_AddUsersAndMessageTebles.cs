﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LiveChat.Migrations
{
    public partial class AddUsersAndMessageTebles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    USER_ID = table.Column<string>(nullable: false),
                    EMAIL = table.Column<string>(nullable: true),
                    NAME = table.Column<string>(nullable: true),
                    PASSWORD = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.USER_ID);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    MESSAGE_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BODY = table.Column<string>(nullable: true),
                    SENDER_ID = table.Column<string>(nullable: false),
                    TIMESTAMP = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.MESSAGE_ID);
                    table.ForeignKey(
                        name: "FK_Messages_Users_SENDER_ID",
                        column: x => x.SENDER_ID,
                        principalTable: "Users",
                        principalColumn: "USER_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "USER_ID", "EMAIL", "NAME", "PASSWORD" },
                values: new object[,]
                {
                    { "e89db13f-7c9b-4907-90f0-83a47b21c50b", "trojan1290@gmail.com", "Влад Андреев", "trojan1290@gmail.com" },
                    { "c52e9cf5-e074-4b3f-84dc-ff5d1bf5f126", "trojan1291@gmail.com", "Роман Ширяев", "trojan1291@gmail.com" },
                    { "0dcfef1c-f2da-4263-960b-812039338c5e", "trojan1292@gmail.com", "Владимир Фомичев", "trojan1292@gmail.com" },
                    { "50eb1c17-0aa6-4b95-ab5c-54d679385e7c", "trojan1293@gmail.com", "Андрей Якушев", "trojan1293@gmail.com" },
                    { "108c99a3-bba6-4b88-8423-07ebb28aee26", "trojan1294@gmail.com", "Николай Гришин", "trojan1294@gmail.com" },
                    { "34a3c4cd-a272-42cf-b267-f11e0e34a7c2", "trojan1295@gmail.com", "Лев Селиверстов", "trojan1295@gmail.com" },
                    { "565c61d1-f409-4732-a34f-5c56113c2a0e", "trojan1296@gmail.com", "Николай Егоров", "trojan1296@gmail.com" },
                    { "cc4b3bde-29ef-4938-b0f8-c3006fbcbe54", "trojan1297@gmail.com", "Тимур Гиряев", "trojan1297@gmail.com" },
                    { "54927a7f-f3da-497e-8ded-49c159860e0a", "trojan1298@gmail.com", "Сергей Дроздов", "trojan1298@gmail.com" },
                    { "c97cb19c-3839-4961-a7a1-c12a6a4c66f8", "trojan1299@gmail.com", "Алексей Кондратьев", "trojan1299@gmail.com" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Messages_SENDER_ID",
                table: "Messages",
                column: "SENDER_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
