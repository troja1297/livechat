﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Claims;
using System.Threading.Tasks;
using LiveChat.Models;
using LiveChat.ViewModels;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Newtonsoft.Json;
using Hub = Microsoft.AspNetCore.SignalR.Hub;

namespace LiveChat.Hubs
{
    [HubName("chathub")]
    public class ChatHub : Hub
    {

        private ApplicationDbContext dbContext;

        public ChatHub(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Метод отправки сообщений, вызывается с клиента.
        /// Рассылает всем клиентам кроме отправителя посланное сообщение.
        /// Рассылает вызываюшему пользователю актуальную информацию о его аккаунте.
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public async Task SendMessage(string message)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                string currentUserId = Context.User.FindFirst("UserId").Value;

                User onlineUser = dbContext.Users.FirstOrDefault(x => x.USER_ID == currentUserId);
                Message messageItem = new Message()
                {
                    BODY = message,
                    SENDER_ID = Context.User.FindFirst("UserId").Value,
                    TIMESTAMP = DateTime.Now,
                    USER_NAME = onlineUser.NAME
                };
                dbContext.Messages.Add(messageItem);

                if (onlineUser != null)
                {
                    onlineUser.LAST_MESSAGE = message;
                    onlineUser.MESSAGE_COUNT++;
                    dbContext.Users.Update(onlineUser ?? throw new InvalidOperationException());
                }

                await dbContext.SaveChangesAsync();
                await Clients.Others.SendAsync("ReceiveMessage", 
                    JsonConvert.SerializeObject(onlineUser, Formatting.Indented),
                    JsonConvert.SerializeObject(messageItem, Formatting.Indented));
                await Clients.Caller.SendAsync("GetActualInformation", JsonConvert.SerializeObject(onlineUser, Formatting.Indented));
            }
        }

        /// <summary>
        /// Возвращает список сообщений с разными аргументами, данными для фильтрации или айди пользователя.
        /// </summary>
        /// <param name="argument">какой либо аргумент</param>
        /// <returns></returns>
        public async Task GetSpecificUserMessages(string argument)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                User specificUser = await dbContext.Users.FirstOrDefaultAsync(x => x.USER_ID == argument);
                string currentUserId = Context.User.FindFirst("UserId").Value;
                User onlineUser = dbContext.Users.FirstOrDefault(x => x.USER_ID == currentUserId);
                if (specificUser != null)
                {
                    await Clients.Caller.SendAsync("GetSpecificUserMessages",
                        JsonConvert.SerializeObject(dbContext.Messages.Where(x => x.SENDER_ID == argument), Formatting.None),
                        JsonConvert.SerializeObject(specificUser, Formatting.None));
                } 
                if (argument == "all")
                {
                    await Clients.Caller.SendAsync("GetMessageList",
                        JsonConvert.SerializeObject(dbContext.Messages, Formatting.None),
                        JsonConvert.SerializeObject(onlineUser, Formatting.None));
                } else if (argument == "today")
                {
                    await Clients.Caller.SendAsync("GetMessageList",
                        JsonConvert.SerializeObject(dbContext.Messages.Where(x => x.TIMESTAMP.Date.Day == DateTime.Now.Date.Day), Formatting.None),
                        JsonConvert.SerializeObject(onlineUser, Formatting.None));
                } else if (argument == "yesterday")
                {
                    DateTime dateTime = DateTime.Now.Date.AddDays(-1);
                    await Clients.Caller.SendAsync("GetMessageList",
                        JsonConvert.SerializeObject(
                            dbContext.Messages.Where(x => x.TIMESTAMP.Date.Day == dateTime.Date.Day), Formatting.None),
                        JsonConvert.SerializeObject(onlineUser, Formatting.None));
                } else
                {
                    Period periods = JsonConvert.DeserializeObject<Period>(argument);

                    string a = "1";
                    if (periods.period1 != null && periods.period2 != null || periods.period1 != null && periods.period2 == null)
                    {
                        var successFirstPeriod = DateTimeOffset.TryParse(periods.period1, CultureInfo.InvariantCulture,
                            DateTimeStyles.AssumeUniversal, out var resultFirs);
                        var successSecondPeriod = DateTimeOffset.TryParse(periods.period2, CultureInfo.InvariantCulture,
                            DateTimeStyles.AssumeUniversal, out var resultSecond);

                        await Clients.Caller.SendAsync("GetMessageList",
                            JsonConvert.SerializeObject(
                                dbContext.Messages.Where(x =>
                                    x.TIMESTAMP.Date >= resultFirs.Date && x.TIMESTAMP.Date <= resultSecond),
                                Formatting.None),
                            JsonConvert.SerializeObject(onlineUser, Formatting.None));
                    }
                }
            }
        }
        
        /// <summary>
        /// Рассылает список пользователей вызывающему при подклбчениии.
        /// Рассылает все остальным оповещение о входе пользователя.
        /// Высылает список сообщений вызывающему.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public override async Task OnConnectedAsync()
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                string currentUserId = Context.User.FindFirst("UserId").Value;

                string users = JsonConvert.SerializeObject(dbContext.Users, Formatting.Indented);

                User onlineUser = dbContext.Users.FirstOrDefault(x => x.USER_ID == currentUserId);
                ConnectedUser.Ids.Add(new Connection()
                {
                    connectionId = Context.ConnectionId,
                    userId = currentUserId
                });

                await Clients.Caller.SendAsync("GetUserList", users, JsonConvert.SerializeObject(ConnectedUser.Ids, Formatting.Indented), currentUserId);
                await Clients.Others.SendAsync("Notify", JsonConvert.SerializeObject(ConnectedUser.Ids, Formatting.Indented));
                await Clients.Caller.SendAsync("GetMessageList", 
                    JsonConvert.SerializeObject(dbContext.Messages, Formatting.None), 
                    JsonConvert.SerializeObject(onlineUser, Formatting.None));
            }
            
            await base.OnConnectedAsync();
        }

        /// <summary>
        /// Рассылает уведомление о выходе из сети пользователя.
        /// </summary>
        /// <param name="exception">принимает ошибку которая привела к отключению клиента</param>
        /// <returns></returns>
        [Authorize]
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                string currentUserId = Context.User.FindFirst("UserId").Value;

                User onlineUser = dbContext.Users.FirstOrDefault(x => x.USER_ID == currentUserId);
                ConnectedUser.Ids.RemoveAll(x => x.connectionId == Context.ConnectionId);

                string users = JsonConvert.SerializeObject(ConnectedUser.Ids, Formatting.Indented);
                List<Connection> list = ConnectedUser.Ids;

                await Clients.Others.SendAsync("Notify", users);
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
